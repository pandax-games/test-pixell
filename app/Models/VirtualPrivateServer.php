<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class VirtualPrivateNetwork
 * @package App\Models
 * @mixin Builder
 */
class VirtualPrivateServer extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['ram_amount'];

    /**
     * Get the server that owns the VPS.
     */
    public function server()
    {
        return $this->belongsTo(Server::class);
    }
}
