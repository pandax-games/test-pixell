<?php

namespace Database\Factories;

use App\Models\Server;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ServerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Server::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        // Available RAM
        $ram = [16384, 32768, 65536, 131072];

        return [
            'name' => Str::random(10),
            'total_ram' => $ram[rand(0, count($ram) - 1)],
        ];
    }
}
