<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login()
    {
        abort(401);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function authenticate(Request $request): JsonResponse
    {
        $credentials = $request->only('email', 'password');

        abort_if(empty($credentials), 401);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            return response()->json(true);
        }

        return response()->json([], 500);
    }
}
