<?php

namespace Database\Seeders;

use App\Models\Server;
use App\Models\VirtualPrivateServer;
use Illuminate\Database\Seeder;

class ServerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create 10 servers
        Server::factory(10)->create()->each(function($server) {
            switch ($server->total_ram) {
                case 32768:
                    $maxVPS = 8;
                    break;

                case 65536:
                    $maxVPS = 16;
                    break;

                case 131072:
                    $maxVPS = 32;
                    break;

                default:
                    $maxVPS = 4;
            }

            // Create 1 to MAX VPS linked to the server
            VirtualPrivateServer::factory([
                'server_id' => $server->id,
            ])->count(rand(1, $maxVPS))->create();
        });
    }
}
