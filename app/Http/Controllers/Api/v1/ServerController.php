<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\ServerStoreRequest;
use App\Http\Requests\ServerUpdateRequest;
use App\Http\Resources\ServerResource;
use App\Models\Server;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ServerController extends Controller
{
    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        return ServerResource::collection( Server::with('vps')->get() );
    }

    /**
     * @param ServerStoreRequest $request
     * @return Server
     */
    public function store(ServerStoreRequest $request): Server
    {
        return Server::create( $request->validated() );
    }

    /**
     * @param Server $server
     * @return ServerResource
     */
    public function edit(Server $server): ServerResource
    {
        return new ServerResource( $server );
    }

    /**
     * @param ServerUpdateRequest $request
     * @param Server $server
     * @return bool|JsonResponse
     */
    public function update(ServerUpdateRequest $request, Server $server)
    {
        // Check that the RAM is available
        if ($request->total_ram - $server->vps->sum('ram_amount') < 0) {
            return response()->json('La RAM non è sufficiente', 422);
        }

        return $server->update( $request->validated() );
    }

    /**
     * @param Server $server
     * @return JsonResponse
     */
    public function destroy(Server $server): JsonResponse
    {
        try {
            $server->delete();
            // TODO: delete VPS?
            return response()->json(true);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function trashed(): AnonymousResourceCollection
    {
        return ServerResource::collection( Server::onlyTrashed()->with('vps')->get() );
    }

    /**
     * @param int $serverId
     * @return JsonResponse
     */
    public function restore(int $serverId): JsonResponse
    {
        try {
            $server = Server::onlyTrashed()->findOrFail($serverId);
            $server->restore();
            // TODO: restore VPS?
            return response()->json(true);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
}
