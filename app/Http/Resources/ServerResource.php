<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ServerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'total_ram' => $this->total_ram,
            'total_ram_formatted' => number_format($this->total_ram, 0, ',', '.'),
            'remaining_ram' => $this->remaining_ram,
            'remaining_ram_formatted' => number_format($this->remaining_ram, 0, ',', '.'),
            'vps_count' => $this->vps->count(),
        ];
    }
}
