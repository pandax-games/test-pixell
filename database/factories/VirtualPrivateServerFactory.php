<?php

namespace Database\Factories;

use App\Models\VirtualPrivateServer;
use Illuminate\Database\Eloquent\Factories\Factory;

class VirtualPrivateServerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = VirtualPrivateServer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'server_id' => rand(1, 10),
            'ram_amount' => rand(0, 4096),
        ];
    }
}
