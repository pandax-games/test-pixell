require('./bootstrap');

//import vue
import Vue from 'vue';

Vue.component('server-list', require('./components/Servers.vue').default);

const app = new Vue({
    el: '#app',
});
