<?php

use App\Http\Controllers\Api\v1\ServerController;
use App\Http\Controllers\Api\v1\VirtualPrivateServerController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'middleware' => 'auth:sanctum'], function() {
    // Servers
    Route::get('servers', [ServerController::class, 'index']);
    Route::post('servers', [ServerController::class, 'store']);
    Route::get('servers/trashed', [ServerController::class, 'trashed']);
    Route::get('servers/{server}', [ServerController::class, 'edit']);
    Route::put('servers/{server}', [ServerController::class, 'update']);
    Route::delete('servers/{server}/delete', [ServerController::class, 'destroy']);
    Route::put('servers/{server}/restore', [ServerController::class, 'restore']);

    // Virtual Private Servers
    Route::get('servers/{server}/vps', [VirtualPrivateServerController::class, 'index']);
    Route::post('servers/{server}/vps', [VirtualPrivateServerController::class, 'store']);
    Route::get('servers/{server}/vps/{vps}', [VirtualPrivateServerController::class, 'edit']);
    Route::put('servers/{server}/vps/{vps}', [VirtualPrivateServerController::class, 'update']);
    Route::delete('servers/{server}/vps/{vps}/delete', [VirtualPrivateServerController::class, 'destroy']);
    Route::put('servers/{server}/vps/{vps}/restore', [VirtualPrivateServerController::class, 'restore']);
});
