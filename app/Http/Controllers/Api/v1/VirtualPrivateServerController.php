<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\VirtualPrivateServerStoreRequest;
use App\Http\Requests\VirtualPrivateServerUpdateRequest;
use App\Http\Resources\VirtualPrivateServerResource;
use App\Models\Server;
use App\Models\VirtualPrivateServer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class VirtualPrivateServerController extends Controller
{
    /**
     * @return AnonymousResourceCollection
     */
    public function index(Server $server): AnonymousResourceCollection
    {
        return VirtualPrivateServerResource::collection(
            VirtualPrivateServer::withTrashed()->where('server_id', $server->id)->get()
        );
    }

    /**
     * @param VirtualPrivateServerStoreRequest $request
     * @param Server $server
     * @return Model|JsonResponse
     */
    public function store(VirtualPrivateServerStoreRequest $request, Server $server)
    {
        // Check that the remaining RAM is enough
        if ($server->remaining_ram - $request->ram_amount < 0) {
            return response()->json('La RAM disponibile non è sufficiente', 422);
        }

        return $server->vps()->create( $request->validated() );
    }

    /**
     * @param Server $server
     * @param VirtualPrivateServer $vps
     * @return VirtualPrivateServerResource
     */
    public function edit(Server $server, VirtualPrivateServer $vps): VirtualPrivateServerResource
    {
        // Check that server and vps are linked
        abort_if($server->id !== $vps->server_id, 404);

        return new VirtualPrivateServerResource($vps);
    }

    /**
     * @param VirtualPrivateServerUpdateRequest $request
     * @param Server $server
     * @param VirtualPrivateServer $vps
     * @return bool|JsonResponse
     */
    public function update(VirtualPrivateServerUpdateRequest $request, Server $server, VirtualPrivateServer $vps)
    {
        // Check that the remaining RAM is enough (adding old amount and subtracting new amount)
        if ($server->remaining_ram + $vps->ram_amount - $request->ram_amount < 0) {
            return response()->json('La RAM disponibile non è sufficiente', 422);
        }

        return $vps->update( $request->validated() );
    }

    /**
     * @param Server $server
     * @return JsonResponse
     */
    public function destroy(Server $server, VirtualPrivateServer $vps): JsonResponse
    {
        // Check that server and vps are linked
        abort_if($server->id !== $vps->server_id, 404);

        try {
            $vps->delete();
            return response()->json(true);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * @param Server $server
     * @param int $vpsId
     * @return JsonResponse
     */
    public function restore(Server $server, int $vpsId): JsonResponse
    {
        try {
            $vps = VirtualPrivateServer::onlyTrashed()->findOrFail($vpsId);

            // Check that server and vps are linked
            abort_if($server->id !== $vps->server_id, 404);

            // Check that the remaining RAM is enough
            if ($server->remaining_ram - $vps->ram_amount < 0) {
                return response()->json('RAM non sufficiente per il ripristino', 422);
            }

            $vps->restore();
            return response()->json(true);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
}
