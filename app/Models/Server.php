<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Server
 * @package App\Models
 * @mixin Builder
 */
class Server extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'total_ram'];

    /**
     * Get the VPS for the server.
     */
    public function vps()
    {
        return $this->hasMany(VirtualPrivateServer::class);
    }

    /**
     * @return mixed
     */
    public function getRemainingRamAttribute()
    {
        return $this->total_ram - $this->vps->sum('ram_amount');
    }
}
